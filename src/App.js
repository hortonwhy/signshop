import "./App.css";
import { React, useState } from "react";
import Cookies from "universal-cookie";
import Nav from "./components/Nav.js";
import Body from "./components/Body.js";
import Footer from "./components/Footer.js";
import "./global.js";

function App() {
  return (
    <div className="flex flex-col h-screen">
      {Nav()}
      {Body()}
      {Footer()}
    </div>
  );
}

export default App;
