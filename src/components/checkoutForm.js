import React from "react";

export default class CheckoutForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      form: {
        line_items: {
          item: {
            quantity: 0,
            selected_options: {
              option: "",
            },
          },
        },
        customer: {
          firstname: "Kip",
          lastname: "Horton",
          email: "hortonwhy@gmail.com",
        },
        shipping: {
          name: "Kip Horton",
          street: "3233 Kip Way",
          town_city: "San Antonio",
          county_state: "TX",
          postal_zip_code: "78251",
          country: "US",
        },
        fulfillment: {
          shipping_method: "",
        },
        billing: {
          name: "",
          street: "",
          town_city: "",
          county_state: "",
          postal_zip_code: "",
          country: "US",
        },
        payment: {
          gateway: "test_gateway",
          card: {
            token: "",
          },
        },
        pay_what_you_want: "",
      },
      checked: true,
      sameBilling: true,
      billingForm: "",
      localeCountries: [],
      localeStates: [],
      localeCountriesBilling: [],
      localeStatesBilling: [],
      test: 0,
    };

    this.handleChangeCustomer = this.handleChangeCustomer.bind(this);
    this.handleChangeShipping = this.handleChangeShipping.bind(this);
    this.handleChangeBilling = this.handleChangeBilling.bind(this);

    this.renderBillingForm = this.renderBillingForm.bind(this);
    this.handleCheck = this.handleCheck.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.ShippingOptions = this.ShippingOptions.bind(this);
  }

  componentDidMount() {
    const getData = async () => {
      global.cartData = await global.cart.getCart();
      await this.setState({ id: global.cartData.id }, () => {
        global.cart.commerce.checkout
          .generateToken(this.state.id, { type: "cart" })
          .then((checkout) => {
            this.setState({ checkoutToken: checkout.id });
          });
      });
    };

    const getCountries = async () => {
      let response = await global.cart.commerce.services.localeListCountries();
      this.setState({
        //localeCountries: Object.entries(response.countries).map(
        //([code, name]) => ({
        //  id: code,
        //  name: name,
        //})
        localeCountries: [{ id: "US", name: "United States" }],
        localeCountriesBilling: Object.entries(response.countries).map(
          ([code, name]) => ({
            id: code,
            name: name,
          })
        ),
      });
    };

    const getStates = async () => {
      let response = await global.cart.commerce.services.localeListSubdivisions(
        this.state.form.shipping.country
      );
      this.setState({
        localeStates: Object.entries(response.subdivisions).map(
          ([code, name]) => ({
            id: code,
            name: name,
          })
        ),
        localeStatesBilling: Object.entries(response.subdivisions).map(
          ([code, name]) => ({
            id: code,
            name: name,
          })
        ),
      });
    };

    getData();
    getCountries().then(() => getStates());
    this.renderBillingForm();
  }
  async ShippingOptions() {
    let response = await global.cart.commerce.checkout
      .getShippingOptions(this.state.checkoutToken, {
        country: this.state.form.shipping.country,
        region: this.state.form.shipping.county_state,
      })
      .then((response) => {
        this.setState((prevState) => ({
          form: {
            ...prevState.form,
            fulfillment: {
              ...prevState.fulfillment,
              fulfillment: response[0].id,
            },
          },
        }));
      });
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.form.shipping.country !== this.state.form.shipping.country) {
      const getStates = async () => {
        let response =
          await global.cart.commerce.services.localeListSubdivisions(
            this.state.form.shipping.country
          );
        this.setState(
          {
            localeStates: Object.entries(response.subdivisions).map(
              ([code, name]) => ({
                id: code,
                name: name,
              })
            ),
          },
          () => {
            const value = this.state.localeStates[0].id;
            //console.log(value);
            this.setState(
              (prevState) => ({
                form: {
                  ...prevState.form,
                  shipping: {
                    ...prevState.form.shipping,
                    county_state: value,
                  },
                },
              }),
              () => {
                //this.ShippingOptions();
              }
            );
          }
        );
      };
      getStates();
    }

    if (prevState.form.billing.country !== this.state.form.billing.country) {
      const getStatesBilling = async () => {
        let response =
          await global.cart.commerce.services.localeListSubdivisions(
            this.state.form.billing.country
          );
        this.setState(
          {
            localeStatesBilling: Object.entries(response.subdivisions).map(
              ([code, name]) => ({
                id: code,
                name: name,
              })
            ),
          },
          () => {
            this.renderBillingForm();
          }
        );
      };
      getStatesBilling();
    }
  }

  async renderBillingForm(targetName) {
    if (!this.state.checked) {
      await this.setState({ sameBilling: false });
      //console.log("not checked");
      await this.setState((prevState) => ({
        billingForm: (
          <>
            <span className="mt-2 block form-input py-2 rounded px-4 mx-2 w-full border-b-4 border-solid divide-neutral-300">
              Billing Information
            </span>

            <label className="block">
              <span className="text-gray-800">Full Name</span>
              <input
                type="text"
                className="mt-2 block form-input py-2 rounded px-4 mx-2 w-full border-2 border-solid divide-neutral-300"
                name="name"
                placeholder="Full Name eg. John Doe"
                value={this.state.form.billing.name}
                onChange={(e) => this.handleChangeBilling(e)}
              />
            </label>

            <label className="block">
              <span className="text-gray-800">Billing Address</span>
              <input
                type="text"
                className="mt-2 block form-input py-2 rounded px-4 mx-2 w-full border-2 border-solid divide-neutral-300"
                name="street"
                placeholder="Billing Address"
                value={this.state.form.billing.street}
                onChange={(e) => this.handleChangeBilling(e)}
              />
            </label>

            <label className="block">
              <span className="text-gray-800">Country</span>
              <select
                className="mt-2 block form-input py-2 rounded px-4 mx-2 w-full border-2 border-solid divide-neutral-300"
                name="country"
                placeholder="Country"
                value={this.state.form.billing.country}
                onChange={this.handleChangeBilling}
              >
                {this.state.localeCountriesBilling.map((country) => {
                  return (
                    <option
                      key={country.id}
                      value={country.id}
                      name="country"
                      onChange={(e) => this.handleChangeBilling(e)}
                    >
                      {country.name}
                    </option>
                  );
                })}
              </select>
            </label>

            <label className="block">
              <span className="text-gray-800">State/Region</span>
              <select
                className="mt-2 block form-input py-2 rounded px-4 mx-2 w-full border-2 border-solid divide-neutral-300"
                name="county_state"
                placeholder="State/Region"
                value={this.state.form.billing.county_state}
                onChange={this.handleChangeBilling}
              >
                {this.state.localeStatesBilling.map((state) => {
                  return (
                    <option
                      key={state.id}
                      value={state.id}
                      name="county_state"
                      onChange={(e) => this.handleChangeBilling(e)}
                    >
                      {state.name}
                    </option>
                  );
                })}
              </select>
            </label>

            <label className="block">
              <span className="text-gray-800">City</span>
              <input
                type="text"
                className="mt-2 block form-input py-2 rounded px-4 mx-2 w-full border-2 border-solid divide-neutral-300"
                name="town_city"
                placeholder="City"
                value={this.state.form.billing.town_city}
                onChange={(e) => this.handleChangeBilling(e)}
              />

              <label className="block py-2">
                <span className="text-gray-800">Postal/Zip Code</span>
                <input
                  type="text"
                  className="mt-2 block form-input py-2 rounded px-4 mx-2 w-full border-2 border-solid divide-neutral-300"
                  name="postal_zip_code"
                  placeholder="Postal/Zip Code"
                  value={this.state.form.billing.postal_zip_code}
                  onChange={(e) => this.handleChangeBilling(e)}
                />
              </label>
            </label>
          </>
        ),
      }));
    } else {
      this.setState({ sameBilling: true });
      this.setState(() => ({
        billingForm: <div id="billingForm" className="py-2"></div>,
      }));
    }
  }

  handleChangeLineItems(event) {
    const value = event.target.value;
    this.setState((prevState) => ({
      form: {
        ...prevState.form,
        line_items: {
          ...prevState.line_items,
          [event.target.name]: value,
        },
      },
    }));
  }

  handleChangeCustomer(event) {
    const value = event.target.value;
    this.setState((prevState) => ({
      form: {
        ...prevState.form,
        customer: {
          ...prevState.form.customer,
          [event.target.name]: value,
        },
      },
    }));
    console.log(this.state.form);
  }

  handleChangeShipping(event) {
    const value = event.target.value;
    this.setState(
      (prevState) => ({
        form: {
          ...prevState.form,
          shipping: {
            ...prevState.form.shipping,
            [event.target.name]: value,
          },
        },
      }),
      () => {
        //this.ShippingOptions();
        //console.log(this.state.form.shipping);
      }
    );
  }

  handleChangeFulfillment(event) {
    const value = event.target.value;
    this.setState((prevState) => ({
      form: {
        ...prevState.form,
        fulfillment: {
          ...prevState.form.fulfillment,
          [event.target.name]: value,
        },
      },
    }));
  }

  handleChangeBilling(event) {
    const value = event.target.value;
    this.setState((prevState) => ({
      form: {
        ...prevState.form,
        billing: {
          ...prevState.form.billing,
          [event.target.name]: value,
        },
      },
    }));
    this.renderBillingForm(event.target.name);
  }

  handleChangePayment(event) {
    const value = event.target.value;
    this.setState((prevState) => ({
      form: {
        ...prevState.form,
        payment: {
          ...prevState.form.payment,
          [event.target.name]: value,
        },
      },
    }));
  }

  handleChangePaymentCard(event) {
    const value = event.target.value;
    this.setState((prevState) => ({
      form: {
        ...prevState.form,
        payment: {
          ...prevState.form.payment,
          card: {
            ...prevState.form.payment.card,
            [event.target.name]: value,
          },
        },
      },
    }));
  }
  async handleCheck() {
    await this.setState({ checked: !this.state.checked });
    //console.log(this.state.checked);
    //await this.setState({ billingForm: this.renderBillingForm() });
    await this.renderBillingForm();
  }

  handleSubmit(event) {
    event.preventDefault();
    const processCheckout = async () => {
      await this.ShippingOptions();
    };
    processCheckout();
    if (this.state.checked) {
      this.setState((prevState) => ({
        form: {
          ...prevState.form,
          billing: {
            ...this.state.form.billing,
            ...this.state.form.shipping,
          },
        },
      }));
    }
    global.cart.commerce.checkout
      .capture(this.state.checkoutToken, this.state.form)
      .then((response) => console.log(response));
    console.log(this.state.form);
    alert(
      "form is" +
        this.state.form.customer.firstname +
        this.state.form.customer.email
    );
  }

  render() {
    return (
      <div className="mx-auto py-12 max-w-md">
        <form onSubmit={this.handleSubmit}>
          <div className="grid grid-cols-1 gap-6">
            <label className="block">
              <span className="text-gray-800">First Name</span>
              <input
                type="text"
                className="mt-2 block form-input py-2 rounded px-4 mx-2 w-full border-2 border-solid divide-neutral-300"
                name="firstname"
                placeholder="First Name"
                value={this.state.form.customer.firstname}
                onChange={this.handleChangeCustomer}
              />
            </label>

            <label>
              <span className="text-gray-800">Last Name</span>
              <input
                type="text"
                name="lastname"
                className="mt-2 block form-input py-2 rounded px-4 mx-2 w-full border-2 border-solid divide-neutral-300"
                value={this.state.form.customer.lastname}
                onChange={this.handleChangeCustomer}
              />
            </label>
            <label>
              <span className="text-gray-800">Email</span>
              <input
                type="text"
                className="mt-2 block form-input py-2 rounded px-4 mx-2 w-full border-2 border-solid divide-neutral-300"
                name="email"
                value={this.state.form.customer.email}
                onChange={this.handleChangeCustomer}
              />
            </label>
            <label>
              <span className="mt-2 block form-input py-2 rounded px-4 mx-2 w-full border-b-4 border-solid divide-neutral-300">
                Shipping Information
              </span>
            </label>

            <label className="block">
              <span className="text-gray-800">Full Name</span>
              <input
                type="text"
                className="mt-2 block form-input py-2 rounded px-4 mx-2 w-full border-2 border-solid divide-neutral-300"
                name="name"
                placeholder="Full Name"
                value={this.state.form.shipping.name}
                onChange={this.handleChangeShipping}
              />
            </label>

            <label className="block">
              <span className="text-gray-800">Address</span>
              <input
                type="text"
                className="mt-2 block form-input py-2 rounded px-4 mx-2 w-full border-2 border-solid divide-neutral-300"
                name="street"
                placeholder="Address"
                value={this.state.form.shipping.street}
                onChange={this.handleChangeShipping}
              />
            </label>

            <label className="block">
              <span className="text-gray-800">Country</span>
              <select
                className="mt-2 block form-input py-2 rounded px-4 mx-2 w-full border-2 border-solid divide-neutral-300"
                name="country"
                placeholder="Country"
                value={this.state.form.shipping.country}
                onChange={this.handleChangeShipping}
              >
                {this.state.localeCountries.map((country) => {
                  return (
                    <option
                      key={country.id}
                      value={country.id}
                      name="country"
                      onChange={this.handleChangeShipping}
                    >
                      {country.name}
                    </option>
                  );
                })}
              </select>
            </label>

            <label className="block">
              <span className="text-gray-800">State</span>
              <select
                className="mt-2 block form-input py-2 rounded px-4 mx-2 w-full border-2 border-solid divide-neutral-300"
                name="county_state"
                value={this.state.form.shipping.county_state}
                onChange={this.handleChangeShipping}
              >
                {this.state.localeStates.map((state) => {
                  return (
                    <option
                      key={state.id}
                      value={state.id}
                      name="county_state"
                      onChange={this.handleChangeShipping}
                    >
                      {state.name}
                    </option>
                  );
                })}
              </select>
            </label>

            <label className="block">
              <span className="text-gray-800">City</span>
              <input
                type="text"
                className="mt-2 block form-input py-2 rounded px-4 mx-2 w-full border-2 border-solid divide-neutral-300"
                name="town_city"
                placeholder="City"
                value={this.state.form.shipping.town_city}
                onChange={this.handleChangeShipping}
              />
            </label>

            <label className="block">
              <span className="text-gray-800">Postal/Zip Code</span>
              <input
                type="text"
                className="mt-2 block form-input py-2 rounded px-4 mx-2 w-full border-2 border-solid divide-neutral-300"
                name="postal_zip_code"
                placeholder="Zip Code"
                value={this.state.form.shipping.postal_zip_code}
                onChange={this.handleChangeShipping}
              />
            </label>

            <label className="inline-flex items-center">
              <input
                className="h-4 w-4 rounded-full shadow checked:shadow-xl"
                type="checkbox"
                checked={this.state.checked}
                onChange={this.handleCheck}
              />
              <span className="ml-2">
                {" "}
                Use Same Information for Billing Address
              </span>
            </label>

            {/* If this.state.checked is false, render more form here*/}
            {this.state.billingForm}

            <input
              type="submit"
              className="border-2 border-solid divide-neutral-300 mx-auto px-4 py-2 hover:bg-slate-300"
              value="Checkout"
            />
          </div>
        </form>
      </div>
    );
  }
}
