import { useState, useEffect } from "react";
import cart from "./cart.js";

let Cart = new cart();
export default function ProductPage(product) {
  let prod_id = "prod_RqEv5xkbbeoZz4";
  const [total, setTotal] = useState(() => {
    return 0;
  });

  function incrementTotal() {
    setTotal((prevTotal) => prevTotal + 1);
  }

  function decrementTotal() {
    setTotal((prevTotal) => (prevTotal > 0 ? prevTotal - 1 : prevTotal));
  }

  if (product) {
    return (
      <div
        className="
	max-w-2xl mx-auto py-16 px-4 sm:py-24 sm:px-6 lg:max-w-7xl lg:px-8"
      >
        <h1 className="text-2xl font-extrabold tracking-tight text-gray-900">
          {product.name}
        </h1>
        <div className="grid grid-rows-3 grid-flow-col gap-1 grid-cols-12">
          <div className="row-span-3 col-span-9 bg-slate-100 border rounded w-4/5">
            <div className="grid grid-rows-2 grid-flow-col gap-4">
              <div className="justify-items-center">
                <img
                  className=" justify-items-center"
                  src={product.imageSrc}
                  alt={product.imageAlt}
                />
              </div>
              <div className="justify-items-center">
                <img
                  className=" justify-items-center"
                  src={product.imageSrc}
                  alt={product.imageAlt}
                />
              </div>
              <div className="justify-items-center">
                <img
                  className="justify-items-center"
                  src={product.imageSrc}
                  alt={product.imageAlt}
                />
              </div>
              <div className="justify-items-center">
                <img
                  className="justify-items-center"
                  src={product.imageSrc}
                  alt={product.imageAlt}
                />
              </div>
            </div>
          </div>
          <div className="col-span-3 font-bold text-gray-900 text-xl">
            {product.name}
            <div className="py-2 text-lg font-normal">
              Price: {product.price}
            </div>

            <div className="py-2 text-lg font-normal">
              <div className="inline-flex">
                <div className="border border-2 py-2 mx-2">
                  <button className="px-2" onClick={decrementTotal}>
                    -
                  </button>
                  <span className="px-2">{total}</span>
                  <button className="px-2" onClick={incrementTotal}>
                    +
                  </button>
                </div>
                <button
                  onClick={() => Cart.add(prod_id, total)}
                  className="border-2 border-solid border-black p-2"
                >
                  Add to Cart
                </button>
              </div>
            </div>
          </div>
          <div className="row-span-2 col-span-3 underline">Description</div>
        </div>
      </div>
    );
  }
}
