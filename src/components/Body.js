import featuredProducts from "./featuredProducts.js";
import productPage from "./productPage.js";
import checkoutPage from "./checkoutPage.js";
import React, { useEffect } from "react";

const products = [
  {
    id: 1,
    name: "Basic Tee",
    href: "/product/1",
    imageSrc:
      "https://tailwindui.com/img/ecommerce-images/product-page-01-related-product-01.jpg",
    imageAlt: "Front of men's Basic Tee in black.",
    price: "$35",
    color: "Black",
  },
  {
    id: 2,
    name: "Basic Tee",
    href: "/product/2",
    imageSrc:
      "https://tailwindui.com/img/ecommerce-images/product-page-01-related-product-01.jpg",
    imageAlt: "Front of men's Basic Tee in black.",
    price: "$35",
    color: "Black",
  },
];

export default function Body() {
  // get the path number within the url
  const queryString = window.location.href;
  const queryArray = queryString.split("/");
  const pageNumber = queryArray[queryArray.length - 1];

  useEffect(() => {
    //console.log(pageNumber);
  }, [pageNumber]);

  // return featured products if no page specified
  const pageIndex = pageNumber - 1;
  if (pageNumber < 1) {
    return <div className="flex-grow">{featuredProducts(products)}</div>;
  } else if (pageNumber === "checkout") {
    return <div className="flex-grow">{checkoutPage()}</div>;
  }

  // otherwise return the specific product page
  return (
    <div className="bg-slate-100 flex-grow">
      {productPage(products[pageIndex])}
    </div>
  );
}
