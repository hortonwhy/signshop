import { useState, useEffect } from "react";
import CheckoutForm from "./checkoutForm.js";
import cart from "./cart.js";
import "../global.js";

export default function Checkout() {
  return (
    <div>
      <CheckoutForm />
    </div>
  );
}
