import React from "react";
import "../global.js";

export default class CartInfo extends React.Component {
  constructor(props) {
    super(props);
    this.state = { cart_total: "", id: "" };
    this.handleUpdate = this.handleUpdate.bind(this);
  }

  componentDidMount() {
    const fetchInfo = async () => {
      const response = await global.cart.getCart();
      this.setState({ cart_total: response.total_items, cart_id: response.id });
    };
    fetchInfo();
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevState.cart_total !== this.state.cart_total) {
      //console.log("cart updated");
    }
  }
  handleUpdate(amtToChange) {
    const fetchInfo = async () => {
      const response = await global.cart.getCart();
      this.setState({ cart_total: response.total_items });
    };
    fetchInfo();
  }

  render() {
    return <div className="py-2">{this.state.cart_total}</div>;
  }
}
