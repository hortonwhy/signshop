import Commerce from "@chec/commerce.js";
import { Component } from "react";

export default class Cart extends Component {
  constructor(props) {
    super(props);
    const commerce = new Commerce(process.env.REACT_APP_COMMERCEJS);
    this.commerce = commerce;
    this.list = this.list.bind(this);
    this.add = this.add.bind(this);
    this.empty = this.empty.bind(this);
  }

  getCart(setCart) {
    return this.commerce.cart.retrieve();
  }

  list() {
    this.commerce.cart.retrieve().then((cart) => console.log(cart));
  }

  add(prod_id, amtToAdd) {
    this.commerce.cart
      .add(prod_id, amtToAdd)
      .then((response) => console.log(response));
  }

  empty() {
    this.commerce.cart.empty().then((response) => console.log(response));
  }
}
